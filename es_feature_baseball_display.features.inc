<?php
/**
 * @file
 * es_feature_baseball_display.features.inc
 */

/**
 * Implements hook_views_api().
 */
function es_feature_baseball_display_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
