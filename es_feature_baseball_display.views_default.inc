<?php
/**
 * @file
 * es_feature_baseball_display.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function es_feature_baseball_display_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'baseball_matches';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'efq_node';
  $view->human_name = 'Baseball matches';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Baseball matches';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'label' => 'label',
    'field_home_game_number' => 'field_home_game_number',
    'field_home_score' => 'field_home_score',
    'field_home_pitcher' => 'field_home_pitcher',
    'field_home_team' => 'field_home_team',
    'field_visiting_team' => 'field_visiting_team',
    'field_visiting_score' => 'field_visiting_score',
    'field_visiting_batters' => 'field_visiting_batters',
    'field_visiting_pitcher' => 'field_visiting_pitcher',
    'field_outs' => 'field_outs',
    'field_park' => 'field_park',
    'entity_id' => 'entity_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'label' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_home_game_number' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_home_score' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_home_pitcher' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_home_team' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_visiting_team' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_visiting_score' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_visiting_batters' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_visiting_pitcher' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_outs' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_park' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'entity_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Entity: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'efq_node';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['link_to_entity'] = 0;
  /* Field: Node: Home game number */
  $handler->display->display_options['fields']['field_home_game_number']['id'] = 'field_home_game_number';
  $handler->display->display_options['fields']['field_home_game_number']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_home_game_number']['field'] = 'field_home_game_number';
  $handler->display->display_options['fields']['field_home_game_number']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Node: Home score */
  $handler->display->display_options['fields']['field_home_score']['id'] = 'field_home_score';
  $handler->display->display_options['fields']['field_home_score']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_home_score']['field'] = 'field_home_score';
  $handler->display->display_options['fields']['field_home_score']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Node: Home starting pitcher */
  $handler->display->display_options['fields']['field_home_pitcher']['id'] = 'field_home_pitcher';
  $handler->display->display_options['fields']['field_home_pitcher']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_home_pitcher']['field'] = 'field_home_pitcher';
  /* Field: Node: Home team */
  $handler->display->display_options['fields']['field_home_team']['id'] = 'field_home_team';
  $handler->display->display_options['fields']['field_home_team']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_home_team']['field'] = 'field_home_team';
  /* Field: Node: Visiting team */
  $handler->display->display_options['fields']['field_visiting_team']['id'] = 'field_visiting_team';
  $handler->display->display_options['fields']['field_visiting_team']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_visiting_team']['field'] = 'field_visiting_team';
  /* Field: Node: Visiting score */
  $handler->display->display_options['fields']['field_visiting_score']['id'] = 'field_visiting_score';
  $handler->display->display_options['fields']['field_visiting_score']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_visiting_score']['field'] = 'field_visiting_score';
  $handler->display->display_options['fields']['field_visiting_score']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Node: Visiting batters */
  $handler->display->display_options['fields']['field_visiting_batters']['id'] = 'field_visiting_batters';
  $handler->display->display_options['fields']['field_visiting_batters']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_visiting_batters']['field'] = 'field_visiting_batters';
  $handler->display->display_options['fields']['field_visiting_batters']['delta_offset'] = '0';
  /* Field: Node: Visiting starting pitcher */
  $handler->display->display_options['fields']['field_visiting_pitcher']['id'] = 'field_visiting_pitcher';
  $handler->display->display_options['fields']['field_visiting_pitcher']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_visiting_pitcher']['field'] = 'field_visiting_pitcher';
  /* Field: Node: Outs */
  $handler->display->display_options['fields']['field_outs']['id'] = 'field_outs';
  $handler->display->display_options['fields']['field_outs']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_outs']['field'] = 'field_outs';
  $handler->display->display_options['fields']['field_outs']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Node: Park */
  $handler->display->display_options['fields']['field_park']['id'] = 'field_park';
  $handler->display->display_options['fields']['field_park']['table'] = 'efq_node';
  $handler->display->display_options['fields']['field_park']['field'] = 'field_park';
  /* Field: Entity: Entity ID */
  $handler->display->display_options['fields']['entity_id']['id'] = 'entity_id';
  $handler->display->display_options['fields']['entity_id']['table'] = 'efq_node';
  $handler->display->display_options['fields']['entity_id']['field'] = 'entity_id';
  $handler->display->display_options['fields']['entity_id']['label'] = 'NID';
  $handler->display->display_options['fields']['entity_id']['link_to_entity'] = 0;
  /* Sort criterion: Node: Home score */
  $handler->display->display_options['sorts']['field_home_score']['id'] = 'field_home_score';
  $handler->display->display_options['sorts']['field_home_score']['table'] = 'efq_node';
  $handler->display->display_options['sorts']['field_home_score']['field'] = 'field_home_score';
  $handler->display->display_options['sorts']['field_home_score']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_home_score']['expose']['label'] = 'Home score';
  /* Filter criterion: Entity: Bundle */
  $handler->display->display_options['filters']['bundle']['id'] = 'bundle';
  $handler->display->display_options['filters']['bundle']['table'] = 'efq_node';
  $handler->display->display_options['filters']['bundle']['field'] = 'bundle';
  $handler->display->display_options['filters']['bundle']['value'] = array(
    'migrate_example_baseball' => 'migrate_example_baseball',
  );
  /* Filter criterion: Entity: Label */
  $handler->display->display_options['filters']['label']['id'] = 'label';
  $handler->display->display_options['filters']['label']['table'] = 'efq_node';
  $handler->display->display_options['filters']['label']['field'] = 'label';
  $handler->display->display_options['filters']['label']['operator'] = 'CONTAINS';
  $handler->display->display_options['filters']['label']['exposed'] = TRUE;
  $handler->display->display_options['filters']['label']['expose']['operator_id'] = 'label_op';
  $handler->display->display_options['filters']['label']['expose']['label'] = 'Label (Contains)';
  $handler->display->display_options['filters']['label']['expose']['operator'] = 'label_op';
  $handler->display->display_options['filters']['label']['expose']['identifier'] = 'label';
  $handler->display->display_options['filters']['label']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  /* Filter criterion: Node: Home starting pitcher */
  $handler->display->display_options['filters']['field_home_pitcher']['id'] = 'field_home_pitcher';
  $handler->display->display_options['filters']['field_home_pitcher']['table'] = 'efq_node';
  $handler->display->display_options['filters']['field_home_pitcher']['field'] = 'field_home_pitcher';
  $handler->display->display_options['filters']['field_home_pitcher']['operator'] = 'STARTS_WITH';
  $handler->display->display_options['filters']['field_home_pitcher']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_home_pitcher']['expose']['operator_id'] = 'field_home_pitcher_op';
  $handler->display->display_options['filters']['field_home_pitcher']['expose']['label'] = 'Home starting pitcher (starts with)';
  $handler->display->display_options['filters']['field_home_pitcher']['expose']['operator'] = 'field_home_pitcher_op';
  $handler->display->display_options['filters']['field_home_pitcher']['expose']['identifier'] = 'field_home_pitcher';
  $handler->display->display_options['filters']['field_home_pitcher']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  $handler->display->display_options['filters']['field_home_pitcher']['delta'] = '';
  /* Filter criterion: Node: Home score */
  $handler->display->display_options['filters']['field_home_score']['id'] = 'field_home_score';
  $handler->display->display_options['filters']['field_home_score']['table'] = 'efq_node';
  $handler->display->display_options['filters']['field_home_score']['field'] = 'field_home_score';
  $handler->display->display_options['filters']['field_home_score']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_home_score']['expose']['operator_id'] = 'field_home_score_op';
  $handler->display->display_options['filters']['field_home_score']['expose']['label'] = 'Home score';
  $handler->display->display_options['filters']['field_home_score']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_home_score']['expose']['operator'] = 'field_home_score_op';
  $handler->display->display_options['filters']['field_home_score']['expose']['identifier'] = 'field_home_score';
  $handler->display->display_options['filters']['field_home_score']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  $handler->display->display_options['filters']['field_home_score']['delta'] = '';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'baseball';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Field Storage Demo';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['baseball_matches'] = $view;

  $view = new view();
  $view->name = 'baseball_search_api';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_baseball';
  $view->human_name = 'Baseball Search API';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Baseball Search API';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_baseball';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  /* Filter criterion: Indexed Node: Content type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_baseball';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'migrate_example_baseball' => 'migrate_example_baseball',
  );
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_baseball';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Fulltext search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'title' => 'title',
    'field_home_batters' => 'field_home_batters',
    'field_home_pitcher' => 'field_home_pitcher',
    'field_home_team' => 'field_home_team',
  );
  /* Filter criterion: Indexed Node: Outs */
  $handler->display->display_options['filters']['field_outs']['id'] = 'field_outs';
  $handler->display->display_options['filters']['field_outs']['table'] = 'search_api_index_baseball';
  $handler->display->display_options['filters']['field_outs']['field'] = 'field_outs';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'baseball-search-api';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Search API Demo';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['baseball_search_api'] = $view;

  return $export;
}
